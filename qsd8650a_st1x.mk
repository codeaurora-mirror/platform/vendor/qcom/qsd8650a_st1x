PRODUCT_PACKAGES := \
    Email \
    IM \
    VoiceDialer \
    SdkSetup


$(call inherit-product, build/target/product/generic.mk)

#Enabling Ring Tones
include frameworks/base/data/sounds/OriginalAudio.mk

# Overrides
PRODUCT_BRAND := qcom
PRODUCT_NAME := qsd8650a_st1x
PRODUCT_DEVICE := qsd8650a_st1x
PRODUCT_LOCALES := hdpi
